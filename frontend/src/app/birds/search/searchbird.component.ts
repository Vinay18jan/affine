import { Component, OnInit } from '@angular/core';
import { BirdService } from 'src/services/bird.service';
import { BirdGetType } from 'src/types/birdgettype';

@Component({
  selector: 'app-searchbird',
  templateUrl: './searchbird.component.html',
  styleUrls: ['./searchbird.component.css']
})
export class SearchbirdComponent implements OnInit {
  public birdResult: BirdGetType;
  constructor(private birdService: BirdService) { 
    this.birdResult = new BirdGetType();
  }

  ngOnInit() {
    this.GetAllBirds();
  }

  delete(birdId){
    if(confirm("Are you sure to delete? "+birdId)) {
      this.birdService.DeleteBird(birdId).subscribe(
        res=>{
          if(res.status==true)
            alert(res.message);
        }
      );
    }
  }
  GetAllBirds() {
    this.birdService.GetAllBirds().subscribe(
      res => {
        
        console.log(this.birdResult);
        if(res.status=200)
          this.birdResult = res.data;

      }
    );
  }

}
