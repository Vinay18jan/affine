import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BirdService } from 'src/services/bird.service';
import { BirdGetType } from 'src/types/birdgettype';

@Component({
  selector: 'app-viewbird',
  templateUrl: './viewbird.component.html',
  styleUrls: ['./viewbird.component.css']
})
export class ViewbirdComponent implements OnInit {
  public _id;
  public birdRes:BirdGetType;
  constructor(private route: ActivatedRoute, private birdService: BirdService) { 
    this.birdRes = new BirdGetType();
    this.route.params.subscribe((params: any) => {
      //console.log(params);
      this._id = (params.id !== undefined)? params.id : '';
    });
  }

  ngOnInit() {
    this.birdService.GetBirdById(this._id).subscribe(
      res=>{
        console.log(res);
        if(res.status==true)
          this.birdRes = res.data;
        else alert(res.message);
      }
    );
  }

}
