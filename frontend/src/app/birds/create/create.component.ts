import { Component, OnInit } from '@angular/core';
import { BirdService } from 'src/services/bird.service';
class BirdConvertor {
  birdname:string;
  familyname:string;
  continentids: string;
  isvisible:boolean;
}

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})


export class CreateComponent implements OnInit {
  public continent;
  public birdname;
  public familyname;
  public isvisible=false;
  public contids;

  constructor(private birdService: BirdService) { }

  ngOnInit() {
    this.GetContinent();
  }

  GetContinent() {
    
    this.birdService.GetContinents().subscribe(
      res=>{
        this.continent = res.data;
      }
    ); 
  }

  addBird(){
    let obj: BirdConvertor = new BirdConvertor();
    obj.birdname = this.birdname;
    obj.familyname = this.familyname;
    obj.continentids = "1,2";
    obj.isvisible = this.isvisible;
    this.birdService.AddBird(obj).subscribe(
      res=>{
        if(res.status==true)
          alert(res.message);
        else alert(res.message);
      },
      err=>{
        alert(err.statusText);
      }
    );
  }

}
