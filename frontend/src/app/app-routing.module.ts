import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchbirdComponent } from './birds/search/searchbird.component';
import { CreateComponent } from './birds/create/create.component';
import { ViewbirdComponent } from './birds/view/viewbird.component';


const routes: Routes = [
  {
    path:'',
    component: SearchbirdComponent
  },
  {
    path:'view/:id',
    component: ViewbirdComponent
  },
  {
    path:'add',
    component: CreateComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
