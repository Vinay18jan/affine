import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SearchbirdComponent } from './birds/search/searchbird.component';
import { CreateComponent } from './birds/create/create.component';
import { HttpClientModule } from '@angular/common/http';
import { BirdService } from 'src/services/bird.service';
import { FormsModule } from '@angular/forms';
import { ViewbirdComponent } from './birds/view/viewbird.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchbirdComponent,
    CreateComponent,
    ViewbirdComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [BirdService],
  bootstrap: [AppComponent]
})
export class AppModule { }
