export class BirdGetType {
    id:number = undefined;
    name:string = undefined;
    Continents:string = undefined;
    family:string = undefined;
    added_on:string = undefined;
    visible: boolean = undefined;
}