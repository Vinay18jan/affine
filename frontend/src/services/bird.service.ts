import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BirdService {
  private _URL = "http://localhost:8000/";

  constructor(private _http: HttpClient) { }

  AddBird(obj){
    return this._http.post<any>(this._URL + "bird", obj);
  }

  GetAllBirds() {
    return this._http.get<any>(this._URL + "birds");
  }

  GetBirdById(id) {
    return this._http.get<any>(this._URL + "bird" + "/" + id);
  }

  DeleteBird(id) {
    return this._http.delete<any>(this._URL + "bird" + "/" + id);
  }

  GetContinents() {
    let headers = new HttpHeaders().set('Content-Type', 'application/json')
    return this._http.get<any>(this._URL + "continents",{headers});
  }
}
